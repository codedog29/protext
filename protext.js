var request = require('request')
var async = require('async')

var getProText = function(text,callback)  {
  var options = {}
  options.url = "http://protext.herokuapp.com/?text=" + text
  options.method = 'GET'
  
  return request(options,function (err,page) {
    var txt = page.body.split("<h1 id='obfuscated'>")[1].split("</h1>")[0]
    return callback(err,txt)
  })
  
}

var decrypt = function (txt,callback) {
  return async.parallel({
    alphabets: function (cb) {
      var alph = 'a bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 '
      return getProText(alph, function (err, encrypted) {
        var encArray = encrypted.split(';')
        var dict = {}
        for(var i = 0 ; i < encArray.length ; i++) {
          dict[encArray[i]] = alph.charAt(i) ;
        }
        return cb(err,dict)
      })
    },
    obfuscated: function (cb) {
      return getProText(txt, cb)
    }
  },function (err, data) {
    var decrypted = ''
    var encrypted = data.obfuscated.split(';')
    var dict = data.alphabets
    for(var i = 0 ; i < encrypted.length ; i++) {
      decrypted += dict[encrypted[i]] ;
    }
    return callback(err,{encrypted: data.obfuscated,decrypted:decrypted})
  })
}


module.exports = {
  getProText: getProText,
  decrypt: decrypt
}