var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var cors = require('cors')
var protext = require('./protext')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cors())

app.get('/ping', function (req, res) {
  return res.send('PONG')
})

app.get('/', function (req, res) {
  var q = req.query.q
  if(q === null || q === undefined || q == '') q = 'The quick brown fox jumps over the lazy dog 1 2 3'
  protext.decrypt(q,function (err,data) {
    res.header('Content-Type', 'text/html');
    var resp = '<h1>Encrypted: ' + data.encrypted + '</h1><h1>Decrypted: ' + data.decrypted + '</h1>'
    return res.send(resp)
  })
})

app.set('port', (process.env.PORT || 3000))

app.listen(app.get('port'), function () {
  console.log('Mash-Up listening on port: ' + app.get('port'))
})

module.exports = app
